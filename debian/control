Source: golang-github-sanity-io-litter
Section: golang
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Anthony Fok <foka@debian.org>
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-sequence-golang,
               golang-any,
               golang-github-stretchr-testify-dev
Testsuite: autopkgtest-pkg-go
Standards-Version: 4.6.1
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-sanity-io-litter
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-sanity-io-litter.git
Homepage: https://github.com/sanity-io/litter
XS-Go-Import-Path: github.com/sanity-io/litter

Package: golang-github-sanity-io-litter-dev
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
         golang-github-stretchr-testify-dev
Description: Pretty printer library for Go data structures to aid in debugging and testing
 Litter is a pretty printer library for Go data structures to aid in
 debugging and testing.
 .
 It's named for the fact that it outputs literals, which you litter
 your output with. As a side benefit, all Litter output is compilable
 Go. You can use Litter to emit data during debug, and it's also really
 nice for "snapshot data" in unit tests, since it produces consistent,
 sorted output.
 .
 Litter was inspired by Spew (https://github.com/davecgh/go-spew), but
 focuses on terseness and readability.
